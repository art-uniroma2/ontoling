/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ONTOLING_CORE.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * ONTOLING_CORE was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about ONTOLING_CORE can be obtained at 
 * http//ai-nlp.info.uniroma2.it/software/...
 *
 */

/*
 * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */
package it.uniroma2.art.ontoling;

import java.io.File;
import java.util.Map;

import it.uniroma2.art.lw.exceptions.LingModelReadException;
import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.PMInitializationException;
import it.uniroma2.art.lw.exceptions.ReplacePlaceholderException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.lw.startup.PlaceholderReplacer;
import it.uniroma2.art.ontoling.exceptions.OntoLingInitializationException;
import it.uniroma2.art.ontoling.ui.GUIAdapter;
import it.uniroma2.art.ontoling.ui.UIReasoner;

public class OntoLingCore {

	private static OntoLingCore ol;
	private UIReasoner uiReasoner;
	private GUIAdapter guiFacade;

	private OntoLingCore(File oSGiDir, String rootBundleDir, boolean subDirPresent, File configFile)
			throws PMInitializationException, LingModelReadException, LinguisticInterfaceLoadException {
		LinguisticWatermarkManager.setLinguisticModel(oSGiDir, rootBundleDir, subDirPresent, configFile);
		LinguisticWatermarkManager.initializeFrame();
		uiReasoner = new UIReasoner();
	}

	public static OntoLingCore initialize(File oSGiDir, String rootBundleDir, boolean subDirPresent,
			File configFile) throws OntoLingInitializationException {
		try {
			ol = new OntoLingCore(oSGiDir, rootBundleDir, subDirPresent, configFile);
		} catch (PMInitializationException e) {
			throw new OntoLingInitializationException(e);
		} catch (LingModelReadException e) {
			throw new OntoLingInitializationException(e);
		} catch (LinguisticInterfaceLoadException e) {
			throw new OntoLingInitializationException(e);
		}
		return ol;
	}

	public static OntoLingCore initialize(File oSGiDir, String rootBundleDir, boolean subDirPresent,
			File configFile, Map<String, String> placeholderValueMap) throws OntoLingInitializationException {
		try {
			String configFilePath = configFile.getAbsolutePath();
			PlaceholderReplacer placeholderReplacer = new PlaceholderReplacer(placeholderValueMap, configFilePath);
			placeholderReplacer.replacePlaceholders();
			ol = new OntoLingCore(oSGiDir, rootBundleDir, subDirPresent, configFile);
		} catch (PMInitializationException e) {
			throw new OntoLingInitializationException(e);
		} catch (LingModelReadException e) {
			throw new OntoLingInitializationException(e);
		} catch (LinguisticInterfaceLoadException e) {
			throw new OntoLingInitializationException(e);
		} catch (ReplacePlaceholderException e) {
			throw new OntoLingInitializationException(e);
		}
		return ol;
	}

	public void setGUIAdapter(GUIAdapter guiAdapter) {
		guiFacade = guiAdapter;
	}

	public GUIAdapter getGUIAdapter() {
		return guiFacade;
	}

	public static OntoLingCore getOntoLingCore() {
		return ol;
	}

	public UIReasoner getUIReasoner() {
		return uiReasoner;
	}

	public LinguisticInterface getSelectedLinguisticResource() {
		return LinguisticWatermarkManager.getSelectedInterface();
	}

	public boolean isLRSuitableForAutomaticEnrichment(LinguisticInterface lint) {
		if (lint != null)
			return (lint.isConceptualized() && (lint.isTaxonomical() || lint.hasGlosses()));
		else
			return false;
	}

}

 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_CORE.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * ONTOLING_CORE was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_CORE can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.uniroma2.art.lw.model.objects.ExplorationObject;
import it.uniroma2.art.lw.model.objects.SearchFilter;
import it.uniroma2.art.lw.model.objects.SearchWord;

import it.uniroma2.art.ontoling.ui.UIAction.ContextMenuSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SemanticIndexTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.GlossTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.ResultTableSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SynonymsListSetter;
import it.uniroma2.art.ontoling.ui.UIAction.RelationExplorerSetter;
import it.uniroma2.art.ontoling.ui.UIAction.ResultTableHeadersSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchFiltersSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchFilterMarker;
import it.uniroma2.art.ontoling.ui.UIAction.SearchWordsSetter;;



/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * @author Andrea Turbati <turbati@info.uniroma2.it>
 */
public class UIActionList extends ArrayList<UIAction> {

    /**
     * 
     */
    private static final long serialVersionUID = -8949184168511084033L;
    
    
    public void addClearSearchTextAction() {
        add(new SearchTextSetter(""));
    }
    
    public void addSetSearchTextAction(String textToSearch) {
        add(new SearchTextSetter(textToSearch) );
    }
    
    public void addClearGlossTextAction() {
        add(new GlossTextSetter(""));
    }
    
    public void addSetGlossTextAction(String glossText) {
        add(new GlossTextSetter(glossText) );
    }
    
    public void addClearConceptTextAction() {
        add(new SemanticIndexTextSetter(""));
    }
    
    public void addSetConceptTextAction(String conceptText) {
        add(new SemanticIndexTextSetter(conceptText) );
    }
    
    public void addClearSynonymsListAction() {
        add(new SynonymsListSetter(new ArrayList<String>(), null));
    }    
    
    public void addSetSynonymsListAction(List<String> synonyms, String word) {
        add(new SynonymsListSetter(synonyms, word));
    }    
    
    public ResultTableSetter createResultTable() {
        return new ResultTableSetter();
    }
    
    public void addClearResultTableAction() {
        add( new ResultTableSetter() );
    }   
    
    public ResultTableSetter addResultTableAction() {
        ResultTableSetter resTable = new ResultTableSetter();
        add( resTable );
        return resTable;
    }    
    
    public void addResultTableAction(ResultTableSetter resTable) {
        add( resTable );
    }
    
    public void addSearchWords(Collection<SearchWord> searchWords){
    	add(new SearchWordsSetter(searchWords));
    }
    
    
    /*
     * INITIALIZATION ACTIONS
     */
    
    public <T extends ExplorationObject>void addSetRelationList(Class<T> type, Collection<T> relations) {
        add( new RelationExplorerSetter<T>(type, relations));
    }
    
    public void addSetSearchFilterList(Collection<SearchFilter> searchFilters) {
    	add( new SearchFiltersSetter(searchFilters));
    }
    
    public void addSetResultTableMetadata(String toolTip, String keyColumn, String valueColumn) {
        add( new ResultTableHeadersSetter(toolTip, keyColumn, valueColumn) );
    }
    
    public void addSetChkBox(SearchFilter filter, boolean value) {
        add( new SearchFilterMarker( filter, value ) );
    }
    
    public void addCMOptionList(CMOptionList cmOmptionList){
    	add( new ContextMenuSetter(cmOmptionList) );
    }
    
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ONTOLING_CORE.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * ONTOLING_PROTEGE was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about ONTOLING_CORE can be obtained at 
 * http//art.uniroma2.it/software/...
 *
 */

package it.uniroma2.art.ontoling.ui;

import it.uniroma2.art.lw.model.objects.ExclusiveExplorationObject;
import it.uniroma2.art.ontoling.ui.UIAction.ContextMenuSetter;
import it.uniroma2.art.ontoling.ui.UIAction.GlossTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.RelationExplorerSetter;
import it.uniroma2.art.ontoling.ui.UIAction.ResultTableHeadersSetter;
import it.uniroma2.art.ontoling.ui.UIAction.ResultTableSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchFilterMarker;
import it.uniroma2.art.ontoling.ui.UIAction.SearchFiltersSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SemanticIndexTextSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SynonymsListSetter;
import it.uniroma2.art.ontoling.ui.UIAction.SearchWordsSetter;

;

public abstract class GUIAdapter {

	public void handleUIActionList(UIActionList al) {
		if (al != null)
			for (UIAction uia : al) {
				Class<? extends UIAction> uiActionClass = uia.getClass();
				if (uiActionClass == SearchTextSetter.class) {
					carryUIAction((SearchTextSetter) uia);
				} else if (uiActionClass == GlossTextSetter.class) {
					carryUIAction((GlossTextSetter) uia);
				} else if (uiActionClass == SemanticIndexTextSetter.class) {
					carryUIAction((SemanticIndexTextSetter) uia);
				} else if (uiActionClass == SynonymsListSetter.class) {
					carryUIAction((SynonymsListSetter) uia);
				} else if (uiActionClass == ResultTableSetter.class) {
					carryUIAction((ResultTableSetter) uia);
				} else if (uiActionClass == ResultTableHeadersSetter.class) {
					carryUIAction((ResultTableHeadersSetter) uia);
				} else if (uiActionClass == RelationExplorerSetter.class) {
					carryUIAction((RelationExplorerSetter) uia);
				} else if (uiActionClass == SearchFiltersSetter.class) {
					carryUIAction((SearchFiltersSetter) uia);
				} else if (uiActionClass == SearchFilterMarker.class) {
					carryUIAction((SearchFilterMarker) uia);
				} else if (uiActionClass == ContextMenuSetter.class) {
					carryUIAction((ContextMenuSetter) uia);
				} else if (uiActionClass == SearchWordsSetter.class) {
					carryUIAction((SearchWordsSetter) uia);
				} else {
					carryUIAction(uia);
				}
			}
	}

	protected abstract void carryUIAction(SearchTextSetter sts);

	protected abstract void carryUIAction(GlossTextSetter gts);

	protected abstract void carryUIAction(SemanticIndexTextSetter cts);

	protected abstract void carryUIAction(SynonymsListSetter sls);

	protected abstract void carryUIAction(ResultTableSetter rts);

	protected abstract void carryUIAction(ResultTableHeadersSetter rths);

	protected abstract <T extends ExclusiveExplorationObject> void carryUIAction(RelationExplorerSetter<T> res);

	protected abstract void carryUIAction(SearchFiltersSetter sfs);

	protected abstract void carryUIAction(SearchFilterMarker sfm);

	protected abstract void carryUIAction(ContextMenuSetter ctxSetter);

	protected abstract void carryUIAction(SearchWordsSetter searchWords);

	protected abstract void carryUIAction(UIAction sts);

}
 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_CORE.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * ONTOLING_CORE was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_CORE can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.ui;

import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.ontoling.model.OntResourceType;

import java.util.ArrayList;



/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * @author Andrea Turbati <turbati@info.uniroma2.it>
 */
public class CMOptionList extends ArrayList<CMOption> {

    /**
     * 
     */
    private static final long serialVersionUID = -7639009093541282254L;

    OntResourceType resType;


    CMOptionList(OntResourceType resType) {
        this.resType = resType;        
    }
    
    
    public OntResourceType getResType() {
        return resType;
    }
    
    public static CMOptionList getCMOptionList(LinguisticInterface lInt, OntResourceType resType) {
        CMOptionList list = new CMOptionList(resType);
        
        list.add(new CMOption.SearchLinguisticResource(resType));
        
        list.add(CMOption.menuSeparator);
        
        list.add(new CMOption.AddTerms(resType));
        
        if (lInt.hasGlosses())
            list.add(new CMOption.AddGloss(resType));
        
        if (lInt.isConceptualized())
            list.add(new CMOption.AddSenseIdToResource(resType));
        
        list.add(CMOption.menuSeparator);
        
        list.add(new CMOption.ChangeNameToSelectedTerm(resType));
        
        if (resType!=OntResourceType.inst) {

            list.add(new CMOption.CreateSubResourceWithSelectedTermAsID(resType));
            
            if (lInt.isTaxonomical() )
                list.add(new CMOption.AddSubResourcesUsingSubConceptsFromLinguisticResources(resType));

        }
        /*
        if(OntoLingCore.getOntoLingCore().isLRSuitableForAutomaticEnrichment(LinguisticWatermarkManager.getSelectedInterface())){
        	list.add(new CMOption.CallLinguisticEnrichment(resType));
        }
        */
        
        return list;
    }
    
     
}

 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is OntoLing.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * OntoLing was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about OntoLing can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */

 /*
  * DynamicMethodsLibrary.java
  *
  * Created on 17 marzo 2005
  */
 
package it.uniroma2.art.ontoling.ui;

import it.uniroma2.art.lw.exceptions.LinguisticResourceAccessException;
import it.uniroma2.art.lw.model.objects.BidirectionalTranslatorConceptualizedLR;
import it.uniroma2.art.lw.model.objects.BidirectionalTranslatorFlatLR;
import it.uniroma2.art.lw.model.objects.BilingualLinguisticInterface;
import it.uniroma2.art.lw.model.objects.LexicalRelation;
import it.uniroma2.art.lw.model.objects.SearchFilter;
import it.uniroma2.art.lw.model.objects.SearchStrategy;
import it.uniroma2.art.lw.model.objects.SearchWord;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.ConceptualizedLR;
import it.uniroma2.art.lw.model.objects.FlatLR;
import it.uniroma2.art.lw.model.objects.LRWithGlosses;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.lw.model.objects.SemanticRelation;
import it.uniroma2.art.ontoling.exceptions.UIActionException;
import it.uniroma2.art.ontoling.model.OntResourceType;
import it.uniroma2.art.ontoling.ui.UIAction.ResultTableSetter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/*
 * this class file is composed of four parts:
 * 1) UI INITIALIZATION                 initializes the GUI with elements depending on the loaded linguistic resource
 * 2) DYNAMIC METHODS ASSIGNMENTS       initializes the method fields in the class with method implementations (see part 4) depending on the loaded linguistic resource 
 * 3) PUBLIC FRONT-END METHODS          these methods wrap the dynamic invocation of the method fields  
 * 4) DYNAMIC METHOD IMPLEMENTATIONS    these are the various implementations of the methods which are assigned to the method fields
 * 
 */

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * @author Andrea Turbati <turbati@info.uniroma2.it>
 */                                                                   
public class UIReasoner {                                           
                                                                    
    public LinguisticInterface lInt;                                
    public String lIntLanguage;                                     
                                                                    
                                                                    
    private int notifiedRelationBehaviour;
    private final static int G=0;
    private final static int S=1;
    
    
    private Method notifiedSearchTerm;
    private Method notifiedSearchConcept;
    private Method notifiedDblClkOnSynonymsTable;
    private Method notifiedSelectedRowOnResultsTable;
    
    private static final String inconsistentErrorMessage = "there is a bug in the system, you should have not incurred in this exception";
    private static final String inconsistentErrorTitle = "Inconsistent State";
    
    
    protected static Log logger = LogFactory.getLog(UIReasoner.class);
    
    public UIActionList initialize(LinguisticInterface lInterface) {    	
        lInt = lInterface;
        lIntLanguage = lInt.getLanguage();
        
        
        /******************************
         * UI INITIALIZATION
         ******************************/
        
        UIActionList al = new UIActionList();
        
        Collection<SemanticRelation> semanticRelationsList = lInt.getSemanticRelations();
        Collection<LexicalRelation> lexicalRelationsList = lInt.getLexicalRelations();
        Collection<SearchStrategy> searchStrategiesList = lInt.getSearchStrategies();
        Collection<SearchFilter> searchFilterList = lInt.getSearchFilters();
        
        al.addSetRelationList(SemanticRelation.class, semanticRelationsList);
        al.addSetRelationList(LexicalRelation.class, lexicalRelationsList);
        al.addSetRelationList(SearchStrategy.class, searchStrategiesList);
        
        if (lInt.isBilingual() && lInt.isFlat())
            if ( ((BilingualLinguisticInterface)lInt).isBidirectional() )
                al.addSetResultTableMetadata("Results Table: left click or press SPACE to select a result", "translation", "backTranslation");
        
        al.addSetSearchFilterList(searchFilterList);
        
        CMOptionList contextMEnuClassList = CMOptionList.getCMOptionList(lInt, OntResourceType.cls);
        CMOptionList contextMEnuPropList = CMOptionList.getCMOptionList(lInt, OntResourceType.prop);
        
        al.addCMOptionList(contextMEnuClassList);
        al.addCMOptionList(contextMEnuPropList);
        
        
        /******************************
         * DYNAMIC METHODS ASSIGNMENTS
         ******************************/
        
        Class<UIReasoner> thisClass = UIReasoner.class; 
        String method;
        
        //choose proper notifiedRelation
        if (lInt.hasGlosses()) notifiedRelationBehaviour = G;
        else notifiedRelationBehaviour = S;
        
        //choose proper notifiedSearchTerm
        if (lInt.isConceptualized())
            if (lInt.hasGlosses()) method = "notifiedSearchTerm_CG";
            else  method = "notifiedSearchTerm_C";
        else method = "notifiedSearchTerm_F";
        try {
        	notifiedSearchTerm = thisClass.getMethod(method, SearchWord.class);
		} catch (Exception e) {
			System.out.println(method);
			System.out.println(notifiedSearchTerm);
			e.printStackTrace();
		}

        //choose proper notifiedSearchConcept
        
		if (lInt.isConceptualized())
            if (lInt.hasGlosses()) method = "notifiedSearchConcept_CG";
            else  method = "notifiedSearchConcept_C";
        else method = "notifiedSearchConcept_F";
        try {
            notifiedSearchConcept = thisClass.getMethod(method, java.lang.String.class);
		} catch (Exception e) {
			System.out.println(method);
			System.out.println(notifiedSearchConcept);
			e.printStackTrace();
		}
		
		
		
        //choose proper notifiedDblClkOnSynonymsTable
        if (lInt.isConceptualized())
            if (lInt.isBilingual())
                if (	((BilingualLinguisticInterface)lInt).isBidirectional()	)   
                    if (lInt.hasGlosses()) method = "notifiedDblClkOnSynonymsTable_CBBG";
                    else method = "notifiedDblClkOnSynonymsTable_CBB";
                else method = "notifiedDblClkOnSynonymsTable_DN";	//bilingual not bidirectional: do nothing
            else if (lInt.hasGlosses()) method = "notifiedDblClkOnSynonymsTable_CG"; //monolingual with gloss
            	 else method = "notifiedDblClkOnSynonymsTable_C"; //monolingual
        //add methods for bilingual conceptualized
        //here follow methods for flat resources
        else if (lInt.isBilingual())
                if (	((BilingualLinguisticInterface)lInt).isBidirectional()	)
                	//	bilingual bidirectional flat: backtranslated results.
                    method = "notifiedDblClkOnSynonymsTable_FBB";
        		else
        		    //	bilingual not bidirectional flat: do nothing.
        		    method = "notifiedDblClkOnSynonymsTable_DN";
    	 	 else
    	 	     method = "notifiedSearchTerm_F";	//monolingual flat: normal search
        try {
            notifiedDblClkOnSynonymsTable = thisClass.getMethod(method, java.lang.String.class);
		} catch (Exception e) {
			System.out.println(method);
			System.out.println(notifiedDblClkOnSynonymsTable);
			e.printStackTrace();
		}
		
		
        //choose proper notifiedSelectedRowOnResultsTable
        if (lInt.isConceptualized())
            if (lInt.hasGlosses()) method = "notifiedSelectedRowOnResultsTable_CG";
            else method = "notifiedSelectedRowOnResultsTable_C";	
        else if (lInt.isBilingual() && ((BilingualLinguisticInterface)lInt).isBidirectional() )
             	method = "notifiedSelectedRowOnResultsTable_BFF";
             else method = "notifiedSelectedRowOnResultsTable_DN";
        try {
        	notifiedSelectedRowOnResultsTable = thisClass.getMethod(method, String.class, String.class);
		} catch (Exception e) {
			System.out.println(method);
			System.out.println(notifiedSelectedRowOnResultsTable);
			e.printStackTrace();
		}
        
        //end of initialization
        System.out.println("\n\nDynamic Methods Initialized");
        
        return al;
        
    }

    
    public UIActionList notifiedSemanticRelation(String semexString, String semanticRelation) throws UIActionException {
        UIActionList al = new UIActionList();
        ResultTableSetter rt = al.addResultTableAction();
        
        //logger.info("semanticRelation = "+semanticRelation+" ,semexString = "+semexString); // da cancellare
        
        SemanticIndex semex;
        try {
            semex = ((ConceptualizedLR)lInt).getConcept(semexString);
            Collection<SemanticIndex> relatedSemexes = lInt.exploreSemanticRelation(semex, lInt.getSemanticRelation(semanticRelation));
            if(relatedSemexes.size() > 0){
            	al.addClearSynonymsListAction();
                al.addClearConceptTextAction();
                al.addClearGlossTextAction();
            }
            Iterator<SemanticIndex> resIterator = relatedSemexes.iterator();
            while (resIterator.hasNext())
            {
                SemanticIndex c = resIterator.next();     
                String rightRow; 
                if (notifiedRelationBehaviour==G)
                    rightRow = populateGlossCell(c);
                else {
                    String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(c);
                    rightRow = concatAllSynonyms(synonyms);
                }                
                rt.addRow(c.getConceptRepresentation(),null,  rightRow );
            }
            return al;
        } catch (SemIndexRetrievalException e) {
            throw new UIActionException("Semantic Index Retrieval Error", "there is no Semantic Index corresponding to: " + semexString, e);
        } catch (LinguisticResourceAccessException e) {
        	throw new UIActionException("Relation Error", "problems in exploring semantic relation: " + semanticRelation, e);
        }        

    }
    
    
    
    public UIActionList notifiedLexicalRelation(String word, String semexString, String lexicalRelation) throws UIActionException {
        UIActionList al = new UIActionList();
        ResultTableSetter rt = al.addResultTableAction();
        
        SemanticIndex semex;
        try {
            semex = ((ConceptualizedLR)lInt).getConcept(semexString);
            Collection<SemanticIndex> relatedSemexesWords = lInt.exploreLexicalRelation(word, semex, lInt.getLexicalRelation(lexicalRelation));
            if(relatedSemexesWords.size() > 0){
            	al.addClearSynonymsListAction();
                al.addClearConceptTextAction();
                al.addClearGlossTextAction();
            }
            Iterator<SemanticIndex> resIterator = relatedSemexesWords.iterator();
            while (resIterator.hasNext())
            {
            	SemanticIndex sw = resIterator.next();     
                String rightRow, centerRow ; 
                if (notifiedRelationBehaviour==G){
                    rightRow = populateGlossCell(sw);
                    centerRow = populateWordCell(sw);
                }
                else {
                    String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(sw);
                    rightRow = concatAllSynonyms(synonyms);
                    centerRow = populateWordCell(sw);
                }                
                rt.addRow(sw.getConceptRepresentation(),centerRow, rightRow );
            }
            return al;
        } catch (SemIndexRetrievalException e) {
            throw new UIActionException("Semantic Index Retrieval Error", "there is no Semantic Index corresponding to: " + semexString, e);            
        } catch (LinguisticResourceAccessException e) {
            throw new UIActionException("Relation Error", "problems in exploring lexical relation: " + lexicalRelation, e);
        }        
	}
	
    
    public UIActionList notifiedSearchStrategy(String word, String searchStrategy, SearchFilter ... searchFilters) throws UIActionException {
        try {
        	
        	//Collection <SearchWord> searchWords = lInt.getSearchWords(word, searchMethodName, searchFilters);
        	Collection <SearchWord> searchWords = lInt.exploreSearchStrategy(word, new SearchStrategy(searchStrategy), searchFilters);
        	UIActionList al = new UIActionList();
        	al.addSearchWords(searchWords);
        	return al; 
        } catch (IllegalArgumentException e) {
            throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
        }
    }
    
    /********************************
     * PUBLIC FRONT-END METHODS
     ********************************/
    
    public UIActionList notifiedSearchField(String word, String searchMethodName, SearchFilter ... searchFilters) throws UIActionException {
        try {
        	
        	//Collection <SearchWord> searchWords = lInt.getSearchWords(word, searchMethodName, searchFilters);
        	Collection <SearchWord> searchWords = lInt.exploreSearchStrategy(word, new SearchStrategy(searchMethodName), searchFilters);
        	UIActionList al = new UIActionList();
        	al.addSearchWords(searchWords);
        	return al; 
        } catch (IllegalArgumentException e) {
            throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
        }
    }
    
    
    public UIActionList notifiedSearchWord(SearchWord searchWord) throws UIActionException {
    	try {
			return (UIActionList)notifiedSearchTerm.invoke(this,searchWord);
		} catch (IllegalArgumentException e) {
			throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
		} catch (IllegalAccessException e) {
			throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
		} catch (InvocationTargetException e) {
			throw new UIActionException( "Resource Access Error", "problems in accessing the linguistic resource", e);
		}
    }
    
    public UIActionList notifiedDblClkOnSynonymsTable(String word) throws UIActionException {
        try {
            return (UIActionList)notifiedDblClkOnSynonymsTable.invoke(this,word);
        } catch (IllegalArgumentException e) {
            throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
        } catch (IllegalAccessException e) {
            throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
        } catch (InvocationTargetException e) {
            throw new UIActionException( "Resource Access Error", "problems in accessing the linguistic resource", e);
        }
    }
    
    
    
    public UIActionList notifiedSearchConcept(String semIndexID) throws UIActionException {
        try {
        	return (UIActionList)notifiedSearchConcept.invoke(this,semIndexID);
        } catch (IllegalArgumentException e) {
            throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
        } catch (IllegalAccessException e) {
            throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
        } catch (InvocationTargetException e) {
            throw new UIActionException( "Resource Access Error", "problems in accessing the linguistic resource", e);
        }
    }
    
    
    public UIActionList notifiedSelectedRowOnResultsTable(String elem, String word) throws UIActionException {
        try {
        	return (UIActionList)notifiedSelectedRowOnResultsTable.invoke(this,elem, word);
        } catch (IllegalArgumentException e) {
            throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
        } catch (IllegalAccessException e) {
            throw new UIActionException( inconsistentErrorTitle, inconsistentErrorMessage, e);
        } catch (InvocationTargetException e) {
            throw new UIActionException( "Resource Access Error", "problems in accessing the linguistic resource", e);
        }
    }
    
    
    
    /********************************
     * DYNAMIC METHOD IMPLEMENTATIONS
     * These are UI methods implementations associated to the front-end methods (depending on LR type) during initialization
     ********************************/
    
    //notifiedSearchTerm_... associated to notifiedSearchField Front-End method 
    
    /**
     * <code>notifiedSearchTerm</code> for Conceptualized Linguistic Resources with Glosses
     * @param term
     */
    public UIActionList notifiedSearchTerm_CG(SearchWord searchWord) {       
        UIActionList al = new UIActionList();
        
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        al.addClearGlossTextAction();
        
        Collection<?> results = ((ConceptualizedLR)lInt).getConcepts(searchWord);
        Iterator<?> resIterator = results.iterator();
        SemanticIndex c;
        ResultTableSetter rt = al.addResultTableAction();
        while (resIterator.hasNext())
        {
            c = (SemanticIndex)resIterator.next();            
            rt.addRow(c.getConceptRepresentation(), searchWord.getName(), populateGlossCell(c) );
        }
        return al;
    }
   
	private String populateGlossCell(SemanticIndex c) {
        String gloss = ((LRWithGlosses)lInt).getConceptGloss(c);
        if (gloss!=null)
            return new String(gloss);
        else {
            String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(c);
            return concatAllSynonyms(synonyms);
        }
    }
        
	private String populateWordCell(SemanticIndex c){
		String word = ((LRWithGlosses)lInt).getWord(c);
		return word;
	}
	
    /**
     * <code>notifiedSearchTerm</code> for Conceptualized Linguistic Resources
     * @param term
     */
    public UIActionList notifiedSearchTerm_C(SearchWord searchWord) {
        
        UIActionList al = new UIActionList();
        al.addClearGlossTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();

        Collection<?> results = ((ConceptualizedLR)lInt).getConcepts(searchWord);
        Iterator<?> resIterator = results.iterator();
        SemanticIndex c;
        ResultTableSetter rt = al.addResultTableAction();
        while (resIterator.hasNext())
        {
            c = (SemanticIndex)resIterator.next();
            String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(c);
            rt.addRow(c.getConceptRepresentation(), searchWord.getName(), concatAllSynonyms(synonyms));
        }
        return al;
    }

    /**
     * <code>notifiedSearchTerm</code> for Flat Linguistic Resources
     * @param term
     */
    public UIActionList notifiedSearchTerm_F(SearchWord searchWord) {
        UIActionList al = new UIActionList();
        al.addClearGlossTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        
        if(searchWord != null){
	        //al.addSetSearchTextAction(searchWord.getName());
	        String[] synonyms = ((FlatLR)lInt).getPotentialSynonyms(searchWord.getName());
	        al.addSetSynonymsListAction(Arrays.asList(synonyms), searchWord.getName());
    }
        //else
        	//al.addSetSearchTextAction("");
        
        return al;
        
        /*
        ProtegeGUI_Facade.notifiedSearchTextFieldModify(term);
        String[] synonyms = ((FlatLR)lInt).getPotentialSynonyms(term);
        ProtegeGUI_Facade.notifiedShowSynonyms(synonyms);
        */
    }    

    
    // notifiedDblClkOnSynonymsTable
    
    /**
     * notifiedDblClkOnSynonymsTable_FBB for Flat Bilingual Bidirectional Translators
     * 
     * @param translatedTerm
     * @return
     */
    public UIActionList notifiedDblClkOnSynonymsTable_FBB(String translatedTerm)  {
    	String[] synonyms = ((BidirectionalTranslatorFlatLR)lInt).getInverseTranslationPotentialSynonyms(translatedTerm);
    	UIActionList al = new UIActionList();
    	al.addClearGlossTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        al.addSetSearchTextAction(translatedTerm);
        
    	ResultTableSetter rt = al.addResultTableAction();
    	for (int i=0;i<synonyms.length;i++)
        {
    	    String[] rereTranslations = ((FlatLR)lInt).getPotentialSynonyms(synonyms[i]);
            rt.addRow(new String(synonyms[i]), null, concatAllSynonyms(rereTranslations));
        }
        return al;
    }

    public UIActionList notifiedDblClkOnSynonymsTable_CBB(String translatedLemma)  {
        UIActionList al = new UIActionList();
        al.addClearSearchTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        al.addSetSearchTextAction(translatedLemma);
        
        Collection<?> results = ((BidirectionalTranslatorConceptualizedLR)lInt).getConceptsFromForeignLemma(translatedLemma);
        Iterator<?> resIterator = results.iterator();
        SemanticIndex c;
        ResultTableSetter rt = al.addResultTableAction();
        while (resIterator.hasNext())
        {
            c = (SemanticIndex)resIterator.next();
            String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(c);
            rt.addRow(new String(c.getConceptRepresentation()), null, concatAllSynonyms(synonyms));
        }
        return al;
    }    

    public UIActionList notifiedDblClkOnSynonymsTable_CBBG(String translatedLemma)  {
        UIActionList al = new UIActionList();
        al.addClearSearchTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        al.addClearGlossTextAction();
        al.addSetSearchTextAction(translatedLemma);
        
        Collection<?> results = ((BidirectionalTranslatorConceptualizedLR)lInt).getConceptsFromForeignLemma(translatedLemma);
        Iterator<?> resIterator = results.iterator();
        SemanticIndex c;
        ResultTableSetter rt = al.addResultTableAction();

        while (resIterator.hasNext())
        {
            c = (SemanticIndex)resIterator.next();
            rt.addRow( new String(c.getConceptRepresentation()), null, populateGlossCell(c) );
        }
        return al;
     }       
        
    public UIActionList notifiedDblClkOnSynonymsTable_DN(String translatedTerm)  {
        UIActionList al = new UIActionList();
        return al;
        //do nothing. No backward translation is possible
    }    
    
    public UIActionList notifiedDblClkOnSynonymsTable_CG(String term){
    	UIActionList al = notifiedSearchTerm_CG(new SearchWord(term));
    	al.addSetSearchTextAction(term);
    	return al;
    }
    
    public UIActionList notifiedDblClkOnSynonymsTable_C(String term){
    	UIActionList al = notifiedSearchTerm_C(new SearchWord(term));
    	al.addSetSearchTextAction(term);
    	return al;
    }
    
    
    // notifiedSearchConcept
    
    public UIActionList notifiedSearchConcept_C(String conceptID) {     
        UIActionList al = new UIActionList();
        ResultTableSetter rt = al.addResultTableAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        al.addClearSearchTextAction();
        SemanticIndex concept;
        try {
            concept = ((ConceptualizedLR)lInt).getConcept(conceptID);
            String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(concept);
            rt.addRow(concept.getConceptRepresentation(), null, populateGlossCell(concept));
            al.addSetConceptTextAction(concept.getConceptRepresentation()); //this is necessary just in case the resource is able to retrieve concept when the conceptID is badly specified
            al.addSetSynonymsListAction(Arrays.asList(synonyms), null);
        } catch (SemIndexRetrievalException e) {
            logger.error("Sense does not exist in the Linguistic Resource, or some other problem occurred");
        }
        return al;
     }

    public UIActionList notifiedSearchConcept_CG(String conceptID) {    	
        UIActionList al = new UIActionList();
        ResultTableSetter rt = al.addResultTableAction();
        al.addClearGlossTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        al.addClearSearchTextAction();
        
        SemanticIndex concept;
        try {
            concept = ((ConceptualizedLR)lInt).getConcept(conceptID);
            String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(concept);
            String gloss = populateGlossCell(concept);
            rt.addRow(concept.getConceptRepresentation(), null, populateGlossCell(concept));
            al.addSetGlossTextAction(gloss);
            al.addSetConceptTextAction(concept.getConceptRepresentation());
            al.addSetSynonymsListAction(Arrays.asList(synonyms), null);
        } catch (SemIndexRetrievalException e) {
            logger.error("Sense does not exist in the Linguistic Resource, or some other problem occurred");
        }
        return al;
    }    

    public void notifiedSearchConcept_F(String conceptID) {
        logger.info("No word sense search on flat resource");
    }
    
    
    // notifiedSelectedRowOnResultsTable
    
    public UIActionList notifiedSelectedRowOnResultsTable_CG(String conceptID, String word) {
    	UIActionList al = new UIActionList();
        al.addClearGlossTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        
        SemanticIndex concept;
        try {
            concept = ((ConceptualizedLR)lInt).getConcept(conceptID);
            String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(concept);
            String gloss = populateGlossCell(concept);
            al.addSetGlossTextAction(gloss);
            al.addSetConceptTextAction(concept.getConceptRepresentation());
            al.addSetSynonymsListAction(Arrays.asList(synonyms), word);
        } catch (SemIndexRetrievalException e) {
            logger.error("Sense does not exist in the Linguistic Resource, or some other problem occurred");
        }
        return al;
    }
    
    public UIActionList notifiedSelectedRowOnResultsTable_C(String conceptID, String word) {     
        UIActionList al = new UIActionList();
        al.addClearGlossTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        
        SemanticIndex concept;
        try {
            concept = ((ConceptualizedLR)lInt).getConcept(conceptID);
            String[] synonyms = ((ConceptualizedLR)lInt).getConceptLexicals(concept);
            al.addSetConceptTextAction(concept.getConceptRepresentation()); //this is necessary just in case the resource is able to retrieve concept when the conceptID is badly specified
            al.addSetSynonymsListAction(Arrays.asList(synonyms), word);
        } catch (SemIndexRetrievalException e) {
            logger.error("Sense does not exist in the Linguistic Resource, or some other problem occurred");
        }
        return al;
     }
    
    public UIActionList notifiedSelectedRowOnResultsTable_BFF(String biTranslatedTerm, String word) {
        UIActionList al = new UIActionList();
        al.addClearGlossTextAction();
        al.addClearSynonymsListAction();
        al.addClearConceptTextAction();
        
        String[] synonyms = ((FlatLR)lInt).getPotentialSynonyms(biTranslatedTerm);
    	al.addSetSynonymsListAction(Arrays.asList(synonyms), word);
    	return al;
    	
    	//ProtegeGUI_Facade.notifiedShowSynonyms(synonyms);   
    }    
    
    public void notifiedSelectedRowOnResultsTable_DN(String translatedTerm) {
        logger.error("this is a do-nothing dynamic method, you should have not incurred in it");
    }
    
    
    
    private String concatAllSynonyms(String [] synonyms) {
        String concat = "";
        if (synonyms.length != 0)
        {    
            concat = synonyms[0];
            for (int i=1;i<synonyms.length;i++)
            {
                concat = concat + ", " + synonyms[i];
            }
        }
        return concat;
    }
    
}

 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_CORE.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * ONTOLING_CORE was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_CORE can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.ui;

import it.uniroma2.art.ontoling.model.OntResourceType;


public abstract class CMOption {

	public static String GLOSS = "<GLOSS>";
	public static String ENTRY_TABLE = "<ENTRY_TABLE>";  //check if it is really useful
	public static String STRING_SEARCH = "<STRING_SEARCH>";
	public static String SENSE_ID = "<SENSE_ID>";
	public static String TERMS = "<TERMS>";
	public static String SLOT = "<SLOT>";
	public static String SELECTED_TERM = "<SELECTED_TERM>"; // It is the element selected from the terms' list returned from the linguistic resource
	public static String SELECTED_RES = "<SELECTED_RES>"; // It is the element selected from the ontology. It can be a class or a property
	public static String LING_ENRICH = "<LING_ENRICH>";
	
	
    public abstract String getLabel();
    
    public static class SearchLinguisticResource extends CMOption {
        
        String label;
                
        SearchLinguisticResource(OntResourceType resType) { //resType = class, property, instance
            this.label = "search Linguistic Resource for " + resType + " : "+SELECTED_RES;
        }
        
        public String getLabel() {
            return label;
        }
        
    }

    public static class MenuSeparator extends CMOption {

        @Override
        public String getLabel() {
        	return "MenuSeparator";
        }
        
        
    };
    
    
    public static class AddTerms extends CMOption {
        
        private String label;
        
        public AddTerms(OntResourceType resType) { //resType = class, property, instance
			this.label = "Add Term(s) "+SELECTED_TERM+" as label "+SLOT+" to "+resType+" "+SELECTED_RES;
		}
                        
        public String getLabel() {
            return label;
        }        
    }
    
    
    public static class AddGloss extends CMOption {
        
        private String label = "Add Gloss";
         
        public AddGloss(OntResourceType resType) { //resType = class, property, instance
			this.label = "Add "+GLOSS+" to "+resType+" "+SELECTED_RES;
		}
        
        public String getLabel() {
            return label;
        }        
    }
        
    
    public static class AddSenseIdToResource extends CMOption {
        
        private String label;
                
        public AddSenseIdToResource(OntResourceType resType) { //resType = class, property, instance
            this.label = "Add "+SENSE_ID+" as label "+SLOT+" for " + resType + " "+SELECTED_RES; 
        }
        
        public String getLabel() {
            return label;
        }        
    }
    
    public static class ChangeNameToSelectedTerm extends CMOption {
        
        private String label;
                
        public ChangeNameToSelectedTerm(OntResourceType resType) { //resType = class, property, instance
            this.label = "Change the name of the " + resType +" from "+SELECTED_RES+" to "+SELECTED_TERM; 
        }
        
        public String getLabel() {
            return label;
        }        
    }   
    
    public static class CreateSubResourceWithSelectedTermAsID extends CMOption {
        
        private String label;
       
        public CreateSubResourceWithSelectedTermAsID(OntResourceType resType) { //resType = class, property, instance
            this.label = "Create sub" + resType + " to "+SELECTED_RES+" using "+SELECTED_TERM+" as ID";
        }
        
        public String getLabel() {
            return label;
        }        
    }  
    
    public static class AddSubResourcesUsingSubConceptsFromLinguisticResources extends CMOption {
        
        private String label;
                
        public AddSubResourcesUsingSubConceptsFromLinguisticResources(OntResourceType resType) { //resType = class, property, instance
            
            this.label = "Generate " + resType.getPluralLabel() + " to "+SELECTED_RES+" using subSenses of "+SELECTED_TERM; 
        }
        
        public String getLabel() {
            return label;
        }        
    }      
    
    public static class CallLinguisticEnrichment extends CMOption {
        
        private String label;
                
        public CallLinguisticEnrichment(OntResourceType resType) { //resType = class, property, instance
            
            this.label = LING_ENRICH+"call LinguisticEnrichment for selected " + resType;
        }
        
        public String getLabel() {
            return label;
        }        
    }  
    
    public static MenuSeparator menuSeparator = new MenuSeparator();
    
}

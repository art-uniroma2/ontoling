 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_CORE.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * ONTOLING_CORE was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_CORE can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.ui;

import it.uniroma2.art.lw.model.objects.SearchFilter;
import it.uniroma2.art.lw.model.objects.SearchWord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class UIAction {

    public static class SearchTextSetter extends UIAction {
        
        private String textToSet;
                
        SearchTextSetter(String textToSet) {
            this.textToSet = textToSet;
        }
        
        public String getTextToSet() {
            return textToSet;
        }
        
    }

    public static class GlossTextSetter extends UIAction {
        
        private String textToSet;
                
        GlossTextSetter(String textToSet) {
            this.textToSet = textToSet;
        }
        
        public String getTextToSet() {
            return textToSet;
        }
        
    }
    
    public static class SemanticIndexTextSetter extends UIAction {
        
        private String textToSet;
                
        SemanticIndexTextSetter(String textToSet) {
            this.textToSet = textToSet;
        }
        
        public String getTextToSet() {
            return textToSet;
        }        
    }
    
    public static class SynonymsListSetter extends UIAction {
        
        private List<String> synonyms;
        private String selWord;
        
        SynonymsListSetter(List<String> synonyms, String selWord) {
            this.synonyms = synonyms;
            this.selWord = selWord;
        }
        
        public List<String> getSynonyms() {
            return synonyms;
        }
        public String getSelectedWord() {
        	return selWord;
        }
    }
        
    public static class ResultTableSetter extends UIAction {        
        
        private ArrayList<String[]> results;
        
        ResultTableSetter() {
            results = new ArrayList<String[]>();
        }
        
        ResultTableSetter(ArrayList<String[]> results) {
            this.results = results;
        }
        
        public void addRow(String first, String second, String third) {
            String[] row = new String[3];
            row[0] = first;
            row[1] = second;
            row[2] = third;
            results.add(row);
        }
        
        public ArrayList<String[]> getResultsList() {
            return results;
        }
        
    }
    
    public static class ResultTableHeadersSetter extends UIAction {        
        
        private String toolTip;
        private String keyColumn;
        private String valueColumn;
        
        
        ResultTableHeadersSetter(String toolTip, String keyColumn, String valueColumn) {
            this.toolTip = toolTip;
            this.keyColumn = keyColumn;
            this.valueColumn = valueColumn;
        }
                
        public String getToolTip() {
            return toolTip;
        }


        public String getKeyColumn() {
            return keyColumn;
        }


        public String getValueColumn() {
            return valueColumn;
        }
        
    }
    
    
    public static class RelationExplorerSetter<T> extends UIAction {
            
        private Collection<T> relations;
        private Class<T> type;
        
        RelationExplorerSetter(Class<T> type, Collection<T> relations) {
            this.setRelations(relations);
            this.setType(type);
        }

        public void setRelations(Collection<T> relations) {
            this.relations = relations;            
        }

        public Collection<T> getRelations() {
            return relations;
        }
        
        public void setType(Class<T> type){
        	this.type = type;
        }
        public Class<T> getType() {
            return type;
        }
        
    }
    
    
    public static class SearchFiltersSetter extends UIAction {        
        
        private Collection<SearchFilter> searchFilters;
        
        SearchFiltersSetter(Collection<SearchFilter> searchFilters) {
            this.setSearchFilters(searchFilters);
        }

        public void setSearchFilters(Collection<SearchFilter> searchFilters) {
            this.searchFilters = searchFilters;
        }

        public Collection<SearchFilter> getSearchFilters() {
            return searchFilters;
        }        
        
    }
    
    public static class SearchFilterMarker extends UIAction {        
        
        private SearchFilter searchFilter;
        boolean value;
        
        SearchFilterMarker(SearchFilter searchFilter, boolean value) {
            this.searchFilter = searchFilter;
            this.value = value;
        }

        public SearchFilter getSearchFilter() {
            return this.searchFilter;            
        }

        public boolean getValue() {
            return this.value;
        }        
        
    }
    
    public static class ContextMenuSetter extends UIAction {
        
        private CMOptionList contextMenu;
        
        ContextMenuSetter(CMOptionList ctxMenu) {
            contextMenu = ctxMenu;
        }
        
        public CMOptionList getContextMenu() {
            return contextMenu;
        }
        
    }
    
    public static class SearchWordsSetter extends UIAction{
    
	    private Collection <SearchWord>searchWords;
	    
	    SearchWordsSetter(Collection<SearchWord> searchWords) {
	    	this.searchWords = searchWords;
	    }
	    
	    public Collection <SearchWord> getSearchWords(){
	    	return searchWords;
	    }
    }
}

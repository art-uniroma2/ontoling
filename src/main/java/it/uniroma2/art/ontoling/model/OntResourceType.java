 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_CORE.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * ONTOLING_CORE was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_CORE can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.model;

public class OntResourceType {

    private String singularLabel;
    private String pluralLabel;
    
    private OntResourceType(String singularLabel, String pluralLabel) {
        this.singularLabel = singularLabel;
        this.pluralLabel = pluralLabel;
    }
    
    public String toString() {
        return singularLabel;
    }
    
    public String getPluralLabel() {
        return pluralLabel;
    }
    
    public static OntResourceType cls = new OntResourceType("class", "classes");
    public static OntResourceType prop = new OntResourceType("property", "properties");
    public static OntResourceType inst = new OntResourceType("instance", "instances");
    
}

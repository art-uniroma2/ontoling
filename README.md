# OntoLing #

OntoLing is a tool allowing for Linguistic Enrichment of Ontologies. It features functionalities for: 

- browsing linguistic resources (thesauri, dictionaries, wordnets...) 

- linguistically enriching ontologies with elements from these linguistic resources Check the main site (currently hosting its old Protege implementation at: art.uniroma2.it/ontoling/ )